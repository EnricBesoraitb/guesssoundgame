package cat.itb.soundquiz.presentation.screens.question;

import androidx.lifecycle.ViewModelProviders;

import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.List;

import cat.itb.soundquiz.R;
import cat.itb.soundquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.soundquiz.domain.Sound;
import cat.itb.soundquiz.domain.Game;
import cat.itb.soundquiz.domain.Question;

public class QuestionFragment extends Fragment {

    private SoundQuizViewModel mViewModel;

    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button soundButton;
    private ProgressBar progressBar;
    private MediaPlayer mediaPlayer;

    public static QuestionFragment newInstance() {
        return new QuestionFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.question_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(SoundQuizViewModel.class);
        Game game = mViewModel.getGame().getValue();
        display(game);

        mViewModel.getTimer().observe(this, this::onTimerChanged);
        mViewModel.getGame().observe(this, this::onGameChanged);
    }

    private void onTimerChanged(Integer i) {
        progressBar.setProgress(i);
        if (progressBar.getProgress() == 0){
            mViewModel.skipQuestion();
        }
    }

    private void onGameChanged(Game game) {
        if (game.isFinished()){
            navigateToGameResult();
        } else {
            display(game);
        }
    }

    private void display(Game game) {
        Question currentQuestion = game.getCurrentQuestion();
        List<Sound> soundList = currentQuestion.getPossibleSounds();

        btn1.setText(soundList.get(0).getName());
        btn2.setText(soundList.get(1).getName());
        btn3.setText(soundList.get(2).getName());
        btn4.setText(soundList.get(3).getName());
        btn5.setText(soundList.get(4).getName());
        btn6.setText(soundList.get(5).getName());

        progressBar.setProgress(1000);



        String fileName = game.getCurrentQuestion().getCorrectSound().getName();
        switch (fileName){
            case "T-rex":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.rex);
                break;
            case "alpaca":
                mediaPlayer = MediaPlayer.create(getContext(),R.raw.alapaca);
                break;
            case "caballo":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.caballo);
                break;
            case "cerdo":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.cerdo);
                break;
            case "ganso":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.ganso);
                break;
            case "gato":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.gato);
                break;
            case "gaviota":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.gaviota);
                break;
            case "grillo":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.grillo);
                break;
            case "leon":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.leon);
                break;
            case "loro":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.loro);
                break;
            case "mapache":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.mapache);
                break;
            case "mono":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.mono);
                break;
            case "pato":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.pato);
                break;
            case "pavo real":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.pavoreal);
                break;
            case "perro":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.perro);
                break;
            case "pollo":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.pollo);
                break;
            case "rana":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.rana);
                break;
            case "vaca":
                mediaPlayer = MediaPlayer.create(getContext(), R.raw.vaca);
                break;



        }


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = getView().findViewById(R.id.progressBar);

        soundButton = getView().findViewById(R.id.questionButton);
        soundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.start();
            }
        });

        btn1 = getView().findViewById(R.id.button1);
        btn1.setTag(0);
        btn1.setOnClickListener(this::answerQuestion);

        btn2 = getView().findViewById(R.id.button2);
        btn2.setTag(1);
        btn2.setOnClickListener(this::answerQuestion);

        btn3 = getView().findViewById(R.id.button3);
        btn3.setTag(2);
        btn3.setOnClickListener(this::answerQuestion);

        btn4 = getView().findViewById(R.id.button4);
        btn4.setTag(3);
        btn4.setOnClickListener(this::answerQuestion);

        btn5 = getView().findViewById(R.id.button5);
        btn5.setTag(4);
        btn5.setOnClickListener(this::answerQuestion);

        btn6 = getView().findViewById(R.id.button6);
        btn6.setTag(5);
        btn6.setOnClickListener(this::answerQuestion);



    }

    private void navigateToGameResult(){
        Navigation.findNavController(getView()).navigate(R.id.question_to_score);
    }

    private void answerQuestion(View view) {
        int number = (Integer) view.getTag();
        mViewModel.answerQuestion(number);
        mediaPlayer.stop();
    }



}
