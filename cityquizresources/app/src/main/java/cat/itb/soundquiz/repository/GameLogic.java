package cat.itb.soundquiz.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cat.itb.soundquiz.data.soundfiledata.SoundsFileSource;
import cat.itb.soundquiz.domain.Sound;
import cat.itb.soundquiz.domain.Game;
import cat.itb.soundquiz.domain.Question;

public class GameLogic {
    SoundsFileSource soundsFileSource;
    Random random = new Random();


    public GameLogic(SoundsFileSource soundsFileSource) {
        this.soundsFileSource = soundsFileSource;
    }

    public Game createGame(int numQuestions, int numAnswers){
        List<Question> questions = createQuestions(numQuestions, numAnswers);
        Game game = new Game(questions);
        return game;
    }

    public Game answerQuestions(Game game, int response){
        Question currentQuestion = game.getCurrentQuestion();
        Sound answer = currentQuestion.getPossibleSounds().get(response);
        game.questionAnswered(answer);
        return game;
    }

    public Game skipQuestion(Game game){
        game.skipQuestion();
        return game;
    }

    private List<Question> createQuestions(int numQuestions, int numAnswers){
        List<Question> questions = new ArrayList<>();
        List<Sound> usedSounds = new ArrayList<>();
        for(int i=0; i<numQuestions; ++i){
            Question question = createQuestion(numAnswers, usedSounds);
            usedSounds.add(question.getCorrectSound());
            questions.add(question);
        }
        return questions;
    }

    private Question createQuestion(int numAnswers, List<Sound> usedSounds){
        List<Sound> answers = soundsFileSource.getNRandomSounds(numAnswers, usedSounds);

        Sound correct = answers.get(random.nextInt(answers.size()));
        Question question = new Question(correct, answers);
        return question;
    }

}
