package cat.itb.soundquiz.domain;

public class Sound {
    String name;


    public Sound(){}

    public Sound(String name, String country) {
        this.name = name;
    }

    public String getName() {
        return name;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sound sound = (Sound) o;

        if (name != null ? !name.equals(sound.name) : sound.name != null) return false;
        return name != null ? name.equals(sound.name) : sound.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
