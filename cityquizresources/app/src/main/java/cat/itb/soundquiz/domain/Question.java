package cat.itb.soundquiz.domain;

import java.util.List;

public class Question {
    Sound correctSound;
    List<Sound> possibleSounds;

    public Question(Sound correctSound, List<Sound> possibleSounds) {
        this.correctSound = correctSound;
        this.possibleSounds = possibleSounds;
    }

    public Sound getCorrectSound() {
        return correctSound;
    }

    public List<Sound> getPossibleSounds() {
        return possibleSounds;
    }
}
