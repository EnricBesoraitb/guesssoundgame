package cat.itb.soundquiz.data.soundfiledata;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cat.itb.soundquiz.domain.Sound;

public class SoundsFileSource {
    List<Sound> sounds;

    public SoundsFileSource() {
        load();
    }

    private void load(){
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<Sound>>(){}.getType();
        sounds = gson.fromJson(SoundJson.json, listType);
    }




    public List<Sound> getSounds() {
        return sounds;
    }

    public List<Sound> getNRandomSounds(int n, List<Sound> skipSounds){
        List<Sound> selection = new ArrayList<>();
        List<Sound> availableSounds = new ArrayList<>(sounds);
        for(Sound sound : skipSounds){
            availableSounds.remove(sound);
        }
        Random random = new Random();
        for(int i=0; i<n; i++){
            int randomInt = random.nextInt(availableSounds.size());
            selection.add(availableSounds.remove(randomInt));
        }
        return selection;
    }


}
