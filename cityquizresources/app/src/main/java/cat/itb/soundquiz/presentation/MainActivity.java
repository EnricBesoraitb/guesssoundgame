package cat.itb.soundquiz.presentation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import cat.itb.soundquiz.R;

import static androidx.navigation.Navigation.findNavController;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
    }
}
