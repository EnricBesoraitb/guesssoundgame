package cat.itb.soundquiz.presentation.screens.score;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cat.itb.soundquiz.R;
import cat.itb.soundquiz.domain.Game;
import cat.itb.soundquiz.presentation.screens.question.SoundQuizViewModel;

public class ScoreFragment extends Fragment {

    private SoundQuizViewModel mViewModel;
    private TextView score;

    public static ScoreFragment newInstance() {
        return new ScoreFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.score_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(SoundQuizViewModel.class);

        mViewModel.getGame().observe(this, this::onChanged);
    }

    private void onChanged(Game game) {
        if(game.isFinished()){
            display(game);
        } else {
            navigateToQuiz();
        }
    }

    public void display(Game game){
        score.setText(game.getScore()+ "");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        score = getView().findViewById(R.id.scoreResult);

        view.findViewById(R.id.playAgainBtn).setOnClickListener(this::playAgain);
    }

    private void playAgain(View view) {
        mViewModel.startQuiz();
    }

    public void navigateToQuiz(){
        Navigation.findNavController(getView()).navigate(R.id.score_to_question);
    }

}
