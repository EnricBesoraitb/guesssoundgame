package cat.itb.soundquiz.domain;

import android.os.SystemClock;

public class Answer {
    Sound sound;
    long time;

    public Answer(Sound sound) {
        this.sound = sound;
        time = SystemClock.elapsedRealtime();
    }

    public Answer(Sound sound, long time) {
        this.sound = sound;
        this.time = time;
    }
}
