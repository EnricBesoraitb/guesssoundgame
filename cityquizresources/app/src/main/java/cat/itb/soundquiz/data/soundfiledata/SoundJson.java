package cat.itb.soundquiz.data.soundfiledata;

public class SoundJson {
    public static final String json = "[{\"name\": \"T-rex\"},\n" +
            "{\"name\": \"alpaca\"},\n" +
            "{\"name\": \"caballo\"},\n" +
            "{\"name\": \"cerdo\"},\n" +
            "{\"name\": \"ganso\"},\n" +
            "{\"name\": \"gato\"},\n" +
            "{\"name\": \"gaviota\"},\n" +
            "{\"name\": \"grillo\"},\n" +
            "{\"name\": \"leon\"},\n" +
            "{\"name\": \"loro\"},\n" +
            "{\"name\": \"mapache\"},\n" +
            "{\"name\": \"mono\"},\n" +
            "{\"name\": \"pato\"},\n" +
            "{\"name\": \"pavo real\"},\n" +
            "{\"name\": \"pavo\"},\n" +
            "{\"name\": \"perro\"},\n" +
            "{\"name\": \"pollo\"},\n" +
            "{\"name\": \"rana\"},\n" +
            "{\"name\": \"vaca\"}]";
}
