package cat.itb.soundquiz.repository;

import cat.itb.soundquiz.data.soundfiledata.SoundsFileSource;

public class RepositoriesFactory {
    private static final SoundsFileSource SOUNDS_FILE_SOURCE = new SoundsFileSource();
    private static final GameLogic gameLogic = new GameLogic(SOUNDS_FILE_SOURCE);

    public static GameLogic getGameLogic() {
        return gameLogic;
    }
}
